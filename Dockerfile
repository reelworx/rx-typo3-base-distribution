FROM php:7.2-apache as dev

RUN a2enmod rewrite

RUN apt-get update && apt-get install -y --no-install-recommends \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libmcrypt-dev \
		libpng-dev \
        libxml2-dev \
        libyaml-dev \
        imagemagick \
        mysql-client \
        vim \
        unzip \
        less

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) intl mysqli gd soap zip


RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) intl mysqli gd soap zip

RUN pecl channel-update pecl.php.net

RUN pecl install yaml && docker-php-ext-enable yaml

COPY config/php.ini $PHP_INI_DIR/php.ini

# -------------- composer ---------------

RUN apt-get install -y --no-install-recommends openssh-client git

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && (php composer-setup.php --quiet --install-dir=/usr/bin --filename=composer ; rm composer-setup.php)


COPY deploy-key /id_rsa

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    mv /id_rsa /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts 2>/dev/null

WORKDIR /var/www/

COPY packages packages
COPY composer.json .

RUN composer install

RUN pecl install xdebug && docker-php-ext-enable xdebug
COPY config/xdebug.ini /usr/local/etc/php/conf.d/

COPY . .

RUN mv config/php.ini /usr/local/etc/php/ && mv config/entrypoint /usr/local/bin/ && chmod a+x /usr/local/bin/entrypoint

ENTRYPOINT ["entrypoint"]
CMD ["apache2-foreground"]

#----------------

FROM php:7.2-apache

RUN apt-get update && apt-get install -y --no-install-recommends \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libmcrypt-dev \
		libpng-dev \
        libxml2-dev \
        imagemagick \
        mysql-client \
        vim \
        less

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) intl mysqli gd soap zip

RUN a2enmod rewrite

WORKDIR /var/www/html/

COPY --from=dev /var/www/packages/ ../packages/
COPY --from=dev /var/www/vendor/ ../vendor/
COPY --from=dev /var/www/html/ .
COPY --from=dev /var/www/database.sql ..
COPY config/entrypoint /usr/local/bin/
COPY config/php.ini /usr/local/etc/php/

RUN chmod a+x /usr/local/bin/entrypoint

ENTRYPOINT ["entrypoint"]
CMD ["apache2-foreground"]
