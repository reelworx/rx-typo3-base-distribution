Reelworx TYPO3 composer base distribution
=========================================

This distribution includes the sitesetup and all the noise around a typical TYPO3 instance.
It uses the `typo3-console/composer-auto-setup` package to automatically set up the instance
with the `composer install` command. (`--dev` only, but this is the default) 


Installation
------------

 - Clone this package
 - Adjust the `.env.install` file
 - Run `composer install`
 - Follow the on-screen instructions


Assets of sitesetup
-------------------

The sitesetup uses `gulp` to create compressed assets.

With `npm`:

* Install [node.js](https://nodejs.org) on your machine
* Run `npm i` to install all dependencies
* Run `npm run gulp release|dev`

With `yarn`:

* Install [node.js](https://nodejs.org) on your machine
* Install [yarn](https://yarnpkg.com) on your machine
* Run `yarn` to install all dependencies
* Run `yarn run gulp release|dev`


Page-not-found handling
-----------------------

The extension ships a custom `[FE][pageNotFound_handling]`-handler that redirects
the user to a page with the id `routing` if an access violation has occurred.
Otherwise the behaviour is like `[FE][pageNotFound_handling] = true`


License
-------

    Copyright by Reelworx GmbH
    https://reelworx.at

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. (GPL.txt)
    If not, see <http://www.gnu.org/licenses/>.
    
    Contact: support@reelworx.at
