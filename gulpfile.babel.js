import gulp from 'gulp';
import plugins from 'gulp-load-plugins';

const $ = plugins();

const Tasks = {
	devContext: true,
	sources: {
		css: 'packages/sitesetup/Resources/Public/Scss/*.scss',
		js: [
			'node_modules/jquery/dist/jquery.js',
			'node_modules/popper.js/dist/umd/popper.js',
			'node_modules/bootstrap/js/dist/util.js',
			'node_modules/bootstrap/js/dist/carousel.js',
			'node_modules/bootstrap/js/dist/modal.js',
			'node_modules/bootstrap/js/dist/dropdown.js',
			'node_modules/bootstrap/js/dist/tooltip.js',
			'node_modules/bootstrap/js/dist/popover.js',
			'node_modules/bootstrap/js/dist/collapse.js',
			//'node_modules/ekko-lightbox/dist/ekko-lightbox.js',
			'packages/sitesetup/Resources/Public/JavaScript/src/lib/*.js',
			'packages/sitesetup/Resources/Public/JavaScript/src/*.js'
		]
	},

	setProduction: function (done) {
		Tasks.devContext = false;
		return done();
	},
	sass: function () {
		let cssPath = 'packages/sitesetup/Resources/Public/Css/';
		let scssPaths = [
			'node_modules/'
		];

		return gulp
			.src(Tasks.sources.css)
			.pipe($.sass({
				outputStyle: Tasks.devContext ? '' : 'compressed',
				includePaths: scssPaths
			}).on('error', $.sass.logError))
			.pipe($.autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			}))
			.pipe(gulp.dest(cssPath));
	},
	js: function () {
		let src = gulp.src(Tasks.sources.js);
		if (!Tasks.devContext) {
			src = src.pipe($.uglify());
		}
		return src
			.pipe($.concat('main.js'))
			.pipe(gulp.dest('packages/sitesetup/Resources/Public/JavaScript/'));
	},
	icons: function() {
		let destPath = 'packages/sitesetup/Resources/Public/fonts/';

		return gulp
			.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
			.pipe(gulp.dest(destPath));
	},
	watch: function (done) {
		gulp.watch(Tasks.sources.css, gulp.series('sass'));
		gulp.watch(Tasks.sources.js, gulp.series('js'));
		return done();
	}
};

gulp.task('sass', Tasks.sass);
gulp.task('js', Tasks.js);
gulp.task('icons', Tasks.icons);
gulp.task('watch', Tasks.watch);

gulp.task('setProduction', Tasks.setProduction);

// combined tasks
gulp.task('release', gulp.series('setProduction', gulp.parallel('sass', 'js', 'icons')));
gulp.task('dev',gulp.parallel('sass', 'js', 'icons'));

gulp.task('default', function() {
	process.stdout.write('\n'
		+ '===========================================\n'
		+ 'Tasks: dev, release, watch\n'
		+ '===========================================\n'
		+ '\n'
	);
});
