<?php declare(strict_types=1);

namespace Reelworx\Sitesetup\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class PageNotFoundHandler
 */
class PageNotFoundHandler
{

    /**
     * @param array $params
     * @param TypoScriptFrontendController $tsfe
     */
    public function handle(array $params, TypoScriptFrontendController $tsfe)
    {
        if (!empty($params['pageAccessFailureReasons']['fe_group'])
            && key($params['pageAccessFailureReasons']['fe_group'])
        ) {
            $getVars = [
                'id' => 'routing',
                'redirect_url' => $params['currentUrl'],
            ];
            if (isset($_GET['L'])) {
                $getVars['L'] = $_GET['L'];
            }
            $queryString = GeneralUtility::implodeArrayForUrl('', $getVars);
            $queryString[0] = '?';
            $baseUrl = GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
            HttpUtility::redirect($baseUrl . $queryString, HttpUtility::HTTP_STATUS_302);
        }
        $tsfe->pageErrorHandler('1', '', $params['reasonText']);
        die;
    }
}
