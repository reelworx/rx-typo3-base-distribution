<?php declare(strict_types=1);

namespace Reelworx\Sitesetup\Hooks;

/**
 * Configure the realurl extension
 */
class RealUrl
{
    /**
     * Add additional configuration on top of the generated configuration
     *
     * @param array $params Parameters to modify
     */
    public function configure(array &$params)
    {
        $domainAliases = [
            'alias.example.com' => 'www.example.com',
        ];

        $rssFeedPageType = 9818;

        $pids = [
            'newsDetailPage' => 123,
            'loginPage' => 123,
        ];

        $overridesToReplace = [
            'fileName' => [
                'index' => [
                    'feed.rss' => [
                        'keyValues' => [
                            'type' => $rssFeedPageType,
                        ],
                    ],
                ],
            ],
            'preVars' => [
                [
                    'GETvar' => 'no_cache',
                    'valueMap' => [
                        'no_cache' => 1,
                    ],
                    'noMatch' => 'bypass',
                ],
                [
                    'GETvar' => 'L',
                    'valueMap' => [
                        'de' => 1,
                    ],
                    'noMatch' => 'bypass',
                ],
            ],
            'fixedPostVars' => [
                'newsDetailConfiguration' => [
                    [
                        'GETvar' => 'tx_news_pi1[action]',
                        'valueMap' => [
                            'detail' => '',
                        ],
                        'noMatch' => 'bypass',
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[controller]',
                        'valueMap' => [
                            'News' => '',
                        ],
                        'noMatch' => 'bypass',
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[news]',
                        'lookUpTable' => [
                            'table' => 'tx_news_domain_model_news',
                            'id_field' => 'uid',
                            'alias_field' => 'title',
                            'addWhereClause' => ' AND NOT deleted',
                            'useUniqueCache' => 1,
                            'useUniqueCache_conf' => [
                                'strtolower' => 1,
                                'spaceCharacter' => '-',
                            ],
                            'languageGetVar' => 'L',
                            'languageExceptionUids' => '',
                            'languageField' => 'sys_language_uid',
                            'transOrigPointerField' => 'l10n_parent',
                            'expireDays' => 180,
                        ],
                    ],
                ],
                $pids['loginPage'] => [
                    [
                        'GETvar' => 'tx_felogin_pi1[forgot]',
                        'valueMap' => [
                            'forgot' => 1,
                        ],
                        'noMatch' => 'bypass',
                    ],
                    [
                        'GETvar' => 'tx_felogin_pi1[redirectReferrer]',
                        'valueMap' => [
                            'noRedirect' => 'off',
                        ],
                        'noMatch' => 'bypass',
                    ],
                    [
                        'GETvar' => 'tx_felogin_pi1[user]',
                    ],
                    [
                        'GETvar' => 'tx_felogin_pi1[forgothash]',
                    ],
                ],
                $pids['newsDetailPage'] => 'newsDetailConfiguration',
            ],
        ];

        $overridesToMerge = [
            'postVarSets' => [
                '_DEFAULT' => [
                    'news' => [
                        [
                            'GETvar' => 'tx_news_pi1[action]',
                            'noMatch' => 'bypass',
                        ],
                        [
                            'GETvar' => 'tx_news_pi1[controller]',
                            'noMatch' => 'bypass',
                        ],
                    ],
                    'datefilter' => [
                        [
                            'GETvar' => 'tx_news_pi1[overwriteDemand][year]',
                        ],
                        [
                            'GETvar' => 'tx_news_pi1[overwriteDemand][month]',
                        ],
                    ],
                    'page' => [
                        [
                            'GETvar' => 'tx_news_pi1[@widget_0][currentPage]',
                        ],
                    ],
                ],
            ],
        ];

        // Apply config to all domains
        foreach ($params['config'] as &$settings) {
            if (is_array($settings)) {
                $settings = array_replace($settings, $overridesToReplace);
                $settings = array_merge_recursive($settings, $overridesToMerge);
            }
        }

        // Apply aliases
        foreach ($domainAliases as $alias => $domain) {
            $params['config'][$alias] = $domain;
        }

        // YAG configuration
        // $params['config']['encodeSpURL_postProc']['yag'] = 'user_Tx_Yag_Hooks_RealUrl->encodeSpURL_postProc';
        // $params['config']['decodeSpURL_preProc']['yag'] = 'user_Tx_Yag_Hooks_RealUrl->decodeSpURL_preProc';
    }
}
