<?php declare(strict_types=1);

namespace Reelworx\Sitesetup\ViewHelpers;

use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\OnlineMediaHelperRegistry;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns a download link for an arbitrary file object
 */
class DownloadUrlViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('file', 'object', 'File to download', true);
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function render()
    {
        $file = $this->arguments['file'];

        $realFile = $file;
        if ($file instanceof FileReference) {
            $realFile = $file->getOriginalFile();
        }

        $onlineHelper = OnlineMediaHelperRegistry::getInstance()->getOnlineMediaHelper($realFile);
        if ($onlineHelper) {
            return '';
        }

        $queryParameterArray = ['eID' => 'dumpFile', 't' => ''];
        if ($file instanceof File) {
            $queryParameterArray['f'] = $realFile->getUid();
            $queryParameterArray['t'] = 'f';
        } elseif ($file instanceof ProcessedFile) {
            $queryParameterArray['p'] = $realFile->getUid();
            $queryParameterArray['t'] = 'p';
        } elseif ($file instanceof FileReference) {
            $queryParameterArray['f'] = $realFile->getUid();
            $queryParameterArray['t'] = 'f';
        } else {
            throw new \RuntimeException('Invalid file object. Type is: ' . get_class($file));
        }
        $queryParameterArray['token'] = GeneralUtility::hmac(implode('|', $queryParameterArray), 'resourceStorageDumpFile');
        $queryParameterArray['download'] = '';
        $uri = 'index.php?' . str_replace('+', '%20', http_build_query($queryParameterArray));
        // Add absRefPrefix
        if (!empty($GLOBALS['TSFE'])) {
            $uri = $GLOBALS['TSFE']->absRefPrefix . $uri;
        }

        return $uri;
    }
}
