<?php declare(strict_types=1);

namespace Reelworx\Sitesetup\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class GalleryColumnsViewHelper
 */
class GalleryColumnsViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('gallery', 'array', 'Gallery file', true);
    }

    /**
     * @return string
     */
    public function render() : string
    {
        $gallery = $this->arguments['gallery'];

        $columns = (int)$gallery['count']['columns'];
        $result = 'col-xs-12';
        if ($columns > 1) {
            $gridSize = 12;

            if ($columns <= 4) {
                $result .= ' col-md-' . ($gridSize/$columns);
            } else {
                $result .= ' col-md-2';
            }
        }

        return $result;
    }
}
