<?php

call_user_func(
    function ($extKey) {
        $settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extKey]);
        if (empty($settings['setPageTSconfig'])) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile($extKey,
                'Configuration/TsConfig/page.ts', 'Default page setup');
        }
    },
    'sitesetup'
);
