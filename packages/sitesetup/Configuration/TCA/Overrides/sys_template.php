<?php
defined('TYPO3_MODE') || die ('Access denied.');

/*
Remove this file if you include the TypoScript in the template fields manually.
Use this syntax for inclusion:

<INCLUDE_TYPOSCRIPT: source="DIR:EXT:sitesetup/Configuration/TypoScript/Constants/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:sitesetup/Configuration/TypoScript/Setup/" extensions="ts">

*/

// Add static TypoScript template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('sitesetup', 'Configuration/TypoScript', 'Site TS');
