mod {
  SHARED {
    # set correct label and flag for default language in BE
    defaultLanguageFlag = gb
    defaultLanguageLabel = English
  }

  # show translated records next to the original ones
  web_layout.defLangBinding = 1

  web_list.enableLocalizationView = activated
  web_list.enableDisplayBigControlPanel = activated
}

TCEMAIN {
  permissions.groupid = 2
  clearCacheCmd = pages
}

TCEFORM.pages {
  layout {
    removeItems = 1,2,3
  }
}

TCEFORM.tt_content {
  section_frame {
    removeItems = 1,5,6,10,11,12,20,21
  }

  layout {
    removeItems = 2,3
    altLabels.1 = Slider
    addItems.51 = Carousel
  }

  header_layout {
    removeItems = 4,5
  }

  imageorient {
    # remove in-text image alignments
    removeItems = 0,8,17,18
  }

  imagecols {
    # remove columns option from textmedia
    removeItems = 5,6,7,8
  }

  frame_class {
    disabled = 1
  }

  space_before_class {
    disabled = 1
  }

  space_after_class {
    keepItems =
    addItems.no-margin = Kein Abstand
    addItems.default-margin = Abstand
  }
}
