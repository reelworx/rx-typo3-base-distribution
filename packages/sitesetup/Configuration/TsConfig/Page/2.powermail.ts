TCEFORM {
  # remove default layouts
  tx_powermail_domain_model_form.css {
    removeItems = layout1, layout2, layout3
  }

  tx_powermail_domain_model_page.css < .tx_powermail_domain_model_form.css
  tx_powermail_domain_model_field.css < .tx_powermail_domain_model_form.css
}
