// single pid for news preview
TCEMAIN.preview {
  tx_news_domain_model_news {
    previewPageId = 123
    useDefaultLanguageRecord = 0
    fieldToParameterMap {
      uid = tx_news_pi1[news]
    }

    additionalGetParameters {
      tx_news_pi1.controller = News
      tx_news_pi1.action = detail
    }
  }
}

// Read more about Linkhandler here:
// https://docs.typo3.org/typo3cms/extensions/core/8.7/Changelog/8.6/Feature-79626-IntegrateRecordLinkHandler.html
TCEMAIN.linkHandler { 
  tx_news {
    configuration {
      table = tx_news_domain_model_news
      storagePid = 2
      hidePageTree = 1
    }
    handler = TYPO3\CMS\Recordlist\LinkHandler\RecordLinkHandler
    label = News
    scanBefore = page
  }
}

tx_news {
  templateLayouts {
    1 = LLL:EXT:sitesetup/Resources/Private/Language/locallang.xlf:news_template_topnews
  }
  module {
    defaultPid.tx_news_domain_model_news = 2
    allowedPage = 2
    preselect {
      sortingField = datetime
      sortingDirection = desc
    }
  }
}

