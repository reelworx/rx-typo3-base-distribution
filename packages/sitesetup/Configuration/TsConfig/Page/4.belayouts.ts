// remove layout "none"
TCEFORM.pages.backend_layout.removeItems = -1
TCEFORM.pages.backend_layout_next_level.removeItems = -1

mod.web_layout.BackendLayouts {
  onecol {
    title = One columns
    config {
      backend_layout {
        colCount = 1
        rowCount = 1
        rows {
          1 {
            columns {
              1 {
                name = Main
                colPos = 0
                colspan = 1
                allowedGridTypes =
              }
            }
          }
        }
      }
    }

    icon = EXT:sitesetup/Resources/Public/Images/BackendLayouts/onecol.png
  }

  twocol {
    title = Two columns
    config {
      backend_layout {
        colCount = 2
        rowCount = 1
        rows {
          1 {
            columns {
              1 {
                name = Left
                colPos = 0
                colspan = 1
                allowedGridTypes =
              }

              2 {
                name = Right
                colPos = 1
                colspan = 1
                allowedGridTypes =
              }
            }
          }
        }
      }
    }

    icon = EXT:sitesetup/Resources/Public/Images/BackendLayouts/twocol.png
  }

  threecol {
    title = Three columns
    config {
      backend_layout {
        colCount = 3
        rowCount = 1
        rows {
          1 {
            columns {
              1 {
                name = Left
                colPos = 0
                colspan = 1
                allowedGridTypes =
              }

              2 {
                name = Center
                colPos = 1
                colspan = 1
                allowedGridTypes =
              }

              3 {
                name = Right
                colPos = 2
                colspan = 1
                allowedGridTypes =
              }
            }
          }
        }
      }
    }

    icon = EXT:sitesetup/Resources/Public/Images/BackendLayouts/threecol.png
  }
}
