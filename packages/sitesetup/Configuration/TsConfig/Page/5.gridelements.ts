tx_gridelements.setup {
  twocol {
    title = LLL:EXT:sitesetup/Resources/Private/Language/locallang.xlf:gridelements.twocol
    description = LLL:EXT:sitesetup/Resources/Private/Language/locallang.xlf:gridelements.description
    icon =
    horizontal = 0
    top_level_layout = 1
    frame = 3
    config {
      colCount = 2
      rowCount = 1
      rows {
        1 {
          columns {
            1 {
              name = LLL:EXT:sitesetup/Resources/Private/Language/locallang.xlf:gridelements.columns.0
              colPos = 0
              allowed = *
              allowedGridTypes =
            }
            2 {
              name = LLL:EXT:sitesetup/Resources/Private/Language/locallang.xlf:gridelements.columns.1
              colPos = 1
              allowed = *
              allowedGridTypes =
            }
          }
        }
      }
    }
  }
}
