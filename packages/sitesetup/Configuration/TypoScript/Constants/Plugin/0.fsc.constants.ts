styles.templates {
  templateRootPath = EXT:sitesetup/Resources/Private/Templates/Content/
  layoutRootPath = EXT:sitesetup/Resources/Private/Layouts/Content/
  partialRootPath = EXT:sitesetup/Resources/Private/Partials/Content/
}

styles.content {
  defaultHeaderType = 2

  textmedia {
    maxW = 600
    maxWInText = 300
    columnSpacing = 10
    rowSpacing = 10
    textMargin = 10
    borderColor = black
    borderWidth = 2
    borderPadding = 0

    linkWrap.width = 800m
    linkWrap.height = 600m
    linkWrap.newWindow = 0
    linkWrap.lightboxEnabled = 1
    linkWrap.lightboxCssClass =
    linkWrap.lightboxRelAttribute =
  }
}
