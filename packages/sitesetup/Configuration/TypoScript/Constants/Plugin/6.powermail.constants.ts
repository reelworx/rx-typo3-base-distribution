plugin.tx_powermail {
  view {
    templateRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Templates/
    partialRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Partials/
    layoutRootPath = EXT:sitesetup/Resources/Private/Extensions/powermail/Layouts/
  }

  settings {
    main {
      confirmation = 0
      optin = 0
      moresteps = 0
    }

    misc.ajaxSubmit = 1
    receiver.overwrite.returnPath = webmaster@example.com
    sender.overwrite.returnPath = webmaster@example.com

    styles.bootstrap {
      numberOfColumns = 1
      fieldWrappingClasses = col-sm-8
      labelClasses = control-label col-sm-4
      offsetClasses = col-sm-offset-4
    }
  }
}
