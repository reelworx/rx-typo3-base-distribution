extensionName = Sitesetup
extensionKey = sitesetup
resourcesPath = EXT:sitesetup/Resources/

# customsubcategory=site=Site

# cat=sitesetup/site/a; type=int+; label=Uid of website root page
rootPageId = 1
rssFeedPageType = 9818

# cat=sitesetup/site/b; type=string; label=List of PIDs linked in the footer
footerMenuPageIDs = 16,17

debug = 0
[applicationContext = Development*]
  debug = 1
[end]

homeUrl = http://example.com/
