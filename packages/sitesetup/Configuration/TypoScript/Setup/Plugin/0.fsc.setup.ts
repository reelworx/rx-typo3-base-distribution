// set default extension name for labels in Fluid
lib.fluidContent.extbase.controllerExtensionName = {$extensionName}

// adjust default lightbox settings
lib.fluidContent.settings.media.popup.linkParams.ATagParams.dataWrap = data-gallery="{field:uid}" data-parent=".image-container"" data-toggle="lightbox" data-title="{file:current:title}" data-footer="{file:current:description}"

#-- rtehtmlarea
lib.parseFunc_RTE {
  nonTypoTagStdWrap.encapsLines {
    remapTag.DIV >
  }

  # responsive tables
  externalBlocks {
    table.stdWrap {
      wrap = <div class="table-responsive">|</div>
      HTMLparser.tags.table.fixAttrib.class {
        default = table
        list = table, table-bordered, table-condensed
      }
    }
  }

  # icons for li
  externalBlocks := addToList(li)
  externalBlocks.ul {
    callRecursive = 1
    callRecursive.tagStdWrap.HTMLparser = 1
    callRecursive.tagStdWrap.HTMLparser.tags.ul {
      fixAttrib.class.default = icon-list
    }
  }
}
