plugin.tx_felogin_pi1._CSS_DEFAULT_STYLE >
plugin.tx_felogin_pi1 {
  welcomeHeader_stdWrap.wrap = <h1>|</h1>
  successHeader_stdWrap.wrap = <h1>|</h1>
  logoutHeader_stdWrap.wrap = <h1>|</h1>
  errorHeader_stdWrap.wrap = <h1>|</h1>
  forgotHeader_stdWrap.wrap = <h1>|</h1>
  changePasswordHeader_stdWrap.wrap = <h1>|</h1>

  exposeNonexistentUserInForgotPasswordDialog = 0
  email_from = no-reply@example.com
  email_fromName = Reelworx
  replyTo =
  newPasswordMinLength = 8
  showForgotPasswordLink = 1
}
