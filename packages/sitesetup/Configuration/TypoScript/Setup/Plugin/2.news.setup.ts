plugin.tx_news {
  settings {
    displayDummyIfNoMedia = 0
    defaultDetailPid = {$news.detailPid}
    tagListPid = {$news.tagListPid}
  }
}

plugin.tx_linkhandler.tx_news {

  // Do not force link generation when the news records are hidden or deleted.
  forceLink = 0

  typolink {
    parameter = {$news.detailPid}
    additionalParams = &tx_news_pi1[news]={field:uid}&tx_news_pi1[controller]=News&tx_news_pi1[action]=detail
    additionalParams.insertData = 1
    useCacheHash = 1
  }
}

tx_news_sitemap.10 {
  select.pidInList = {$news.sysfolderPid}
  select.max = 25
  renderObj.10.10.10.typolink.parameter = {$news.detailPid}
}
