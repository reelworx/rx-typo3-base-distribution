# gridelements
tt_content.gridelements_pi1.20.10.setup {
  # ID of grid element
  twocol < lib.gridelements.defaultGridSetup
  twocol {
    cObject = FLUIDTEMPLATE
    cObject {
      file = {$resourcesPath}Private/Extensions/gridelements/twocol.html
    }
  }
}
