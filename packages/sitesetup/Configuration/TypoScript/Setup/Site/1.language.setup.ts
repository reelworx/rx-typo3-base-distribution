# General config
config {
  linkVars = L(0-1)
  sys_language_mode = content_fallback; 0
  sys_language_overlay = 1
}

# English
config {
  locale_all = en_US.utf8,en_US.UTF-8,en_GB.utf8,en_GB.UTF-8,en_US,en_GB,en
  language = en
  sys_language_uid = 0
  sys_language_isocode_default = en
  htmlTag_setParams = lang="en" itemscope="itemscope" itemtype="http://schema.org/WebPage"
}

# German
[globalVar = GP:L = 1]
  config {
    locale_all = de_AT.utf8,de_AT.UTF-8,de_DE.utf8,de_DE.UTF-8,de_AT,de_DE,de
    language = de
    sys_language_uid = 1
    htmlTag_setParams = lang="de" itemscope="itemscope" itemtype="http://schema.org/WebPage"
  }
[global]

plugin.tx_rlmplanguagedetection_pi1 {
  useOneTreeMethod = 1
  defaultLang = en
}
