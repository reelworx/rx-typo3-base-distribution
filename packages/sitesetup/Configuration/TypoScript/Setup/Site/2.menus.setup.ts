lib.menu = HMENU
lib.menu {
  entryLevel = 0

  1 = TMENU
  1 {
    expAll = 1
    wrap = <ul class="nav navbar-nav">|</ul>

    # prepend link to home
    stdWrap.prepend = TEXT
    stdWrap.prepend {
      htmlSpecialChars = 1
      data = leveltitle:0
      typolink.parameter.data = leveluid:0

      wrap = <li>|</li>
      wrap.override = <li class="active">|<span class="sr-only"> (current)</span></li>
      wrap.override {
        if.value.data = leveluid:0
        if.equals.data = TSFE:id
      }
    }

    NO = 1
    NO.stdWrap.htmlSpecialChars = 1
    NO.wrapItemAndSub = <li>|</li>

    CUR = 1
    CUR.stdWrap.htmlSpecialChars = 1
    CUR.wrapItemAndSub = <li class="active">|</li>
    CUR.linkWrap = |<span class="sr-only"> (current)</span>

    IFSUB = 1
    IFSUB {
      stdWrap.htmlSpecialChars = 1
      wrapItemAndSub = <li class="dropdown">|</li>
      ATagParams = class="dropdown-toggle" data-toggle="dropdown"
    }

    ACTIFSUB < .IFSUB
    ACTIFSUB.wrapItemAndSub = <li class="dropdown active">|</li>
    CURIFSUB < .ACTIFSUB
  }

  2 = TMENU
  2 {
    wrap = <ul class="dropdown-menu">|</ul>
    NO = 1
    NO {
      stdWrap.htmlSpecialChars = 1
      wrapItemAndSub = <li>|</li>
    }

    CUR = 1
    CUR.stdWrap.htmlSpecialChars = 1
    CUR.wrapItemAndSub = <li class="active">|</li>
  }
}

lib.submenu = HMENU
lib.submenu {
  entryLevel = 1
  1 = TMENU
  1 {
    wrap = <ul id="subnavigation" class="nav nav-pills nav-stacked">|</ul>
    NO {
      wrapItemAndSub = <li>|</li>
      stdWrap.htmlSpecialChars = 1
    }

    ACT = 1
    ACT {
      wrapItemAndSub = <li class="active">|</li>
      stdWrap.htmlSpecialChars = 1
    }
  }
}

lib.footerMenu = HMENU
lib.footerMenu {
  special = list
  special.value = {$footerMenuPageIDs}
  includeNotInMenu = 1

  1 = TMENU
  1 {
    noBlur = 1

    NO = 1
    NO {
      stdWrap.htmlSpecialChars = 1
      allWrap = | |*| &nbsp;&#124;&nbsp;| |*|
    }
  }
}
