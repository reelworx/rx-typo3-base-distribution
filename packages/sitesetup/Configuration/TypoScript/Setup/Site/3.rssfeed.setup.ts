# Add (localized) feed link to normal FE output
page.headerData.14 = TEXT
page.headerData.14 {
  value.typolink.parameter = {$rootPageId},{$rssFeedPageType}
  value.typolink.returnLast = url
  htmlSpecialChars = 1
  wrap = <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="|" />
}

#-- RSS feed from news extension
[globalVar = TSFE:type = {$rssFeedPageType}]
  config {
    disableAllHeaderCode = 1
    xhtml_cleaning = none
    admPanel = 0
    debug = 0
    disablePrefixComment = 1
    metaCharset = utf-8
    additionalHeaders.10.header = Content-Type:application/rss+xml;charset=utf-8
    absRefPrefix = {$plugin.tx_news.rss.channel.link}
  }

  pageNewsRSS = PAGE
  pageNewsRSS {
    typeNum = {$rssFeedPageType}
    10 < tt_content.list.20.news_pi1
    10 {
      switchableControllerActions {
        News {
          1 = list
        }
      }

      settings < plugin.tx_news.settings
      settings {
        limit = 10
        detailPid = {$news.detailPid}
        startingpoint = {$news.sysfolderPid}
        format = xml
      }
    }
  }
[global]
