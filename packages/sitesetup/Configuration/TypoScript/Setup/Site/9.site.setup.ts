// No indexing, we're not using a search
config.index_enable = 0

// don't show page name in page title on root page
[page | is_siteroot = 1]
  config.noPageTitle = 1
[end]

lib.mainContent < styles.content.get

lib.sidebar < styles.content.get
lib.sidebar.select.where = colPos=1
lib.sidebar.slide = -1
lib.sidebar.slide.collect = -1
lib.sidebar.slide.collectReverse = 1

page = PAGE
page.typeNum = 0

page.meta {
  viewport = width=device-width, initial-scale=1.0
  X-UA-Compatible = IE=edge
  X-UA-Compatible.httpEquivalent = 1
  keywords.field = keywords
  description.field = description
}

# attention: the files browserconfig.xml and manifest.json need to be adjusted manually too (paths)!!
page.headerData.7 = TEXT
page.headerData.7.value.insertData = 1
page.headerData.7.value (

<link rel="apple-touch-icon" sizes="57x57" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-57x57.png}"/>
<link rel="apple-touch-icon" sizes="60x60" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-60x60.png}"/>
<link rel="apple-touch-icon" sizes="72x72" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-72x72.png}"/>
<link rel="apple-touch-icon" sizes="76x76" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-76x76.png}"/>
<link rel="apple-touch-icon" sizes="114x114" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-114x114.png}"/>
<link rel="apple-touch-icon" sizes="120x120" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-120x120.png}"/>
<link rel="apple-touch-icon" sizes="144x144" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-144x144.png}"/>
<link rel="apple-touch-icon" sizes="152x152" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-152x152.png}"/>
<link rel="apple-touch-icon" sizes="180x180" href="{path:{$resourcesPath}Public/Favicons/apple-touch-icon-180x180.png}"/>
<link rel="icon" type="image/png" href="{path:{$resourcesPath}Public/Favicons/favicon-32x32.png}" sizes="32x32"/>
<link rel="icon" type="image/png" href="{path:{$resourcesPath}Public/Favicons/favicon-194x194.png}" sizes="194x194"/>
<link rel="icon" type="image/png" href="{path:{$resourcesPath}Public/Favicons/favicon-96x96.png}" sizes="96x96"/>
<link rel="icon" type="image/png" href="{path:{$resourcesPath}Public/Favicons/android-chrome-192x192.png}" sizes="192x192"/>
<link rel="icon" type="image/png" href="{path:{$resourcesPath}Public/Favicons/favicon-16x16.png}" sizes="16x16"/>
<link rel="manifest" href="{path:{$resourcesPath}Public/Favicons/manifest.json}"/>
<link rel="mask-icon" href="{path:{$resourcesPath}Public/Favicons/safari-pinned-tab.svg}" color="#5bbad5"/>
<link rel="shortcut icon" href="{path:{$resourcesPath}Public/Favicons/favicon.ico}"/>
<meta name="apple-mobile-web-app-title" content="Reelworx"/>
<meta name="application-name" content="Reelworx"/>
<meta name="msapplication-TileColor" content="#b91d47"/>
<meta name="msapplication-TileImage" content="{path:{$resourcesPath}Public/Favicons/mstile-144x144.png}"/>
<meta name="msapplication-config" content="{path:{$resourcesPath}Public/Favicons/browserconfig.xml}"/>
<meta name="theme-color" content="#ffffff"/>

)

# opengraph sample (note: ext:news generates those too for arcticles)
page.headerData.40 = TEXT
page.headerData.40.value.insertData = 1
page.headerData.40.value (

<meta property="og:title" content="Website of Company" />
<meta property="og:type" content="sport" />
<meta property="og:url" content="http://www.example.at/" />
<meta property="og:site_name" content="Company GmbH" />
<meta property="og:description" content="{$page:description}" />

)

page.includeCSS {
  styles = {$resourcesPath}Public/Css/main.css
}

page.includeJSFooterlibs {
  mainjs = {$resourcesPath}Public/JavaScript/main.js
}

page.10 = FLUIDTEMPLATE
page.10 {
  extbase.controllerExtensionName = {$extensionName}

  templateRootPaths.10 = {$resourcesPath}Private/Templates/
  layoutRootPaths.10 = {$resourcesPath}Private/Layouts/
  partialRootPaths.10 = {$resourcesPath}Private/Partials/

  # select template based on backend layout
  templateName.cObject = CASE
  templateName.cObject {
    key.data = pagelayout

    default = TEXT
    default.value = Subpage

    pagets__onecol = TEXT
    pagets__onecol.value = Index
  }

  settings {
    rootPid = {$rootPageId}
  }
}

page.1001 =< plugin.tx_rlmplanguagedetection_pi1
