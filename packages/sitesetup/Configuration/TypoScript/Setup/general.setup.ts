# Basic config
config {
  intTarget = _self
  extTarget = _blank
  spamProtectEmailAddresses_atSubst = <span style="display:none">&bull;</span>@<span style="display:none">&bull;</span>
  spamProtectEmailAddresses = -3
  doctype = html5
  metaCharset = utf-8
  pageTitleFirst = 1
  headerComment = Created by Reelworx GmbH

  debug = 0
  admPanel = 0
  # disabled until https://forge.typo3.org/issues/63815
  sendCacheHeaders = 0
  enableContentLengthHeader = 1
  disablePrefixComment = 1

  concatenateCss = 1
  concatenateJs = 1
  moveJsFromHeaderToFooter = 1
}

[globalVar = LIT:1 = {$debug}]
  config {
    admPanel = 1
    sendCacheHeaders = 0
    disablePrefixComment = 0
    concatenateJs = 0
    concatenateCss = 0
  }
[global]
