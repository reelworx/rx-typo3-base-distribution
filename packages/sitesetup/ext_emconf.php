<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Site Setup',
    'description' => 'Complete setup of a website. Includes all TypoScript, TSconfig and resources.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'uploadfolder' => 0,
    'author' => 'Markus Klein',
    'author_email' => 'support@reelworx.at',
    'author_company' => 'Reelworx GmbH',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
            'scheduler' => '8.7.0-8.7.99',
            'fluid_styled_content' => '8.7.0-8.7.99',
            'recycler' => '8.7.0-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [
            'form' => '8.7.0-8.7.99',
            'gridelements' => '8.0.0-8.99.99',
            'realurl' => '2.2.0-2.99.99',
            'news' => '6.1.0-6.99.99',
            'rx_shariff' => '10.0.0-10.99.99',
            'dd_googlesitemap' => '2.1.4-2.99.99',
            'sr_language_menu' => '6.4.0-6.99.99',
            'rlmp_language_detection' => '8.0.0-8.99.99',
            'powermail' => '4.4.0-4.99.99',
            'extractor' => '1.5.0-1.99.99',
            't3monitoring_client' => '1.0.0-1.99.99',
            'fs_media_gallery' => '1.4.7-1.99.99',
            'fal_securedownload' => '2.1.3-2.99.99',
            'unroll' => '2.0.0-2.99.99',
            'ke_search' => '2.6.0-2.99.99'
        ],
    ],
];
