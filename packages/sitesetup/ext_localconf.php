<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {
        $settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extKey]);

        if (!empty($settings['setPageTSconfig'])) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
                '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extKey . '/Configuration/TsConfig/Page/" extensions="ts">'
            );
        }

        if (!empty($settings['setUserTSconfig'])) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
                '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extKey . '/Configuration/TsConfig/User/" extensions="ts">'
            );
        }

        /** @var \TYPO3\CMS\Core\Configuration\ConfigurationManager $configManager */
        $configManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ConfigurationManager::class);

        $originalConfig = $sysConfig = $configManager->getLocalConfiguration();

        // even though this should actually always be true, there seems to be a core bug, where sysconfig is null
        if (is_array($sysConfig)) {
            $sysConfig['BE']['debug'] = 0;
            $sysConfig['BE']['sessionTimeout'] = 3600;
            $sysConfig['BE']['compressionLevel'] = 5;
            $sysConfig['BE']['versionNumberInFilename'] = true;
            $sysConfig['BE']['loginSecurityLevel'] = 'normal';
            $sysConfig['FE']['debug'] = 0;
            $sysConfig['FE']['compressionLevel'] = 5;
            $sysConfig['FE']['pageNotFound_handling'] = 'USER_FUNCTION:' . Reelworx\Sitesetup\Hooks\PageNotFoundHandler::class . '->handle';
            $sysConfig['FE']['versionNumberInFilename'] = 'embed';
            $sysConfig['FE']['loginSecurityLevel'] = 'normal';
            $sysConfig['FE']['hidePagesIfNotTranslatedByDefault'] = true;
            $sysConfig['SYS']['sitename'] = 'sitesetup';
            $sysConfig['SYS']['sqlDebug'] = 0;
            $sysConfig['SYS']['displayErrors'] = 0;
            $sysConfig['SYS']['enableDeprecationLog'] = '';
            $sysConfig['SYS']['doNotCheckReferer'] = true;
            $sysConfig['SYS']['ddmmyy'] = 'd.m.Y';
            $sysConfig['SYS']['UTF8filesystem'] = true;
            $sysConfig['SYS']['systemLocale'] = 'en_US.utf8';
            $sysConfig['SYS']['exceptionalErrors'] = 4096;
            $sysConfig['MAIL']['transport'] = 'smtp';
            $sysConfig['MAIL']['transport_smtp_server'] = 'localhost:25';
            $sysConfig['MAIL']['defaultMailFromAddress'] = 'support@reelworx.at';
            $sysConfig['MAIL']['defaultMailFromName'] = 'support@reelworx.at';

            $applicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
            if ($applicationContext->isDevelopment()) {
                $sysConfig['SYS']['sitename'] = $sysConfig['SYS']['sitename'] . ' (Development)';
                $sysConfig['SYS']['enableDeprecationLog'] = 'file';
                $sysConfig['SYS']['exceptionalErrors'] = 12290;
                $sysConfig['SYS']['sqlDebug'] = 1;
                $sysConfig['SYS']['displayErrors'] = 1;

                $sysConfig['BE']['lockIP'] = 0;

                $sysConfig['MAIL']['transport'] = 'mbox';
                $sysConfig['MAIL']['transport_mbox_file'] = PATH_site . 'mails.txt';
            }

            if (!empty(\TYPO3\CMS\Core\Utility\ArrayUtility::arrayDiffAssocRecursive($sysConfig, $originalConfig))) {
                $configManager->writeLocalConfiguration($sysConfig);
            }
        }

        // configure realurl
        $realUrlConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl']);
        if (!$realUrlConfig['enableAutoConf']) {
            $realUrlConfig['enableAutoConf'] = 1;
            $configManager->setLocalConfigurationValueByPath('EXT/extConf/realurl', serialize($realUrlConfig));
        }
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration'][] = \Reelworx\Sitesetup\Hooks\RealUrl::class . '->configure';

        // configure scheduler
        $schedulerConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['scheduler']);
        if ($schedulerConfig['showSampleTasks']) {
            $schedulerConfig['showSampleTasks'] = 0;
            $schedulerConfig['enableBELog'] = 0;
            $configManager->setLocalConfigurationValueByPath('EXT/extConf/scheduler', serialize($schedulerConfig));
        }

        // style BE login
        $logo = 'EXT:' . $extKey . '/Resources/Public/Images/logo.png';
        $beImage = 'EXT:' . $extKey . '/Resources/Public/Images/belogin_background.jpg';
        $backendConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);
        if ($backendConfig['loginBackgroundImage'] !== $beImage) {
            $backendConfig['loginLogo'] = $logo;
            $backendConfig['loginBackgroundImage'] = $beImage;

            $configManager->setLocalConfigurationValueByPath('EXT/extConf/backend', serialize($backendConfig));
        }

        $GLOBALS['TYPO3_CONF_VARS']['LOG']['Reelworx']['RxShariff']['writerConfiguration'] = [
            \TYPO3\CMS\Core\Log\LogLevel::WARNING => [
                \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
                    'logFile' => 'typo3temp/var/logs/shariff-4oi45.log'
                ]
            ]
        ];
        $GLOBALS['TYPO3_CONF_VARS']['LOG']['TYPO3']['CMS']['Frontend']['processorConfiguration'] = [
            \TYPO3\CMS\Core\Log\LogLevel::ERROR => [
                \TYPO3\CMS\Core\Log\Processor\WebProcessor::class => []
            ]
        ];
    },
    $_EXTKEY
);
